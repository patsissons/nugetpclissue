# README #

This is a small project that illustrates and resolves the [NuGet Issue #4013](https://nuget.codeplex.com/workitem/4013). The issue involves using replacement tokens to inject content into the `*.nuspec` file for a portable class project.

This issue revolves around PCL assembly loading when performing a *NuGet Pack* operation. This is most simply reproduced by creating an extension method inside of a portable class project. The extension method requires that System.Core is loaded, which will fail and cause the metadata loading to fail. Without the metadata loaded, the replacement tokens are left empty.

## Solution ##

A fix has been created for this issue, but it is scheduled for the 3.0.0 release. That means until 3.0.0 is released, this issue will remain something we must deal with.

If you want to continue extracting metadata form a PCL project, then you can use the included patched NuGet.exe (version `2.8.1.0`) as a replacement to `.nuget\NuGet.exe` in your solution.

Here is a direct link to the [most recent patched NuGet executable](https://bitbucket.org/patsissons/nugetpclissue/src/HEAD/.nuget-patched/NuGet.exe?at=master)

## Patched NuGet Executable Updates ##

The patched executable is not the most recent version available. It is somewhat recent, but there are still a number of commits since it has been patched. It is very unlikely that I will run into the scenario where I require a more up to date patched NuGet executable (than version `2.8.1.0`). However, if anyone finds that there is a good reason can either re-compile the most recent version with [the already merged patch](https://nuget.codeplex.com/SourceControl/network/forks/StephenCleary/Issue4013/contribution/6798#!/tab/changes) and create a pull request, Or simply send me a message (or create an issue) that explains what version and why you need it and I can probably recompile the NuGet executable and update this repository in a reasonable amount of time.

## Using the Patched Executable ##

A quick warning to those using this patched executable. As I was building this reproduction project, I ran into an issue where I simply added the patched executable to the existing `.nuget` directory and gave it a new name (`NuGet-patched.exe`). This resulted in unexpected behaviour, due to what I assume is assembly loading issues. Essentially, even though a **Pack** operation is called using the patched executable, the pack will fail for the same reasons that an unpatched executable would. This is why you must completely replace the original executable, or alternatively store the patched executable in a different location and with the name unchanged.
