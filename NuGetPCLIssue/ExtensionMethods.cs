﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NuGetPCLIssue
{
    public static partial class ExtensionMethods
    {
        // https://nuget.codeplex.com/workitem/4013
        // http://stackoverflow.com/questions/23676953/error-creating-nuget-package-for-portable-class-library
        // A bug in NuGet 2.8.x exists, until NuGet 3.x.x is released a patched executable must be used that contains the fix
        // Use the .nuget-patched\NuGet.exe as a work-around (run Pack-patched.bat to verify)
        // Use the .nuget-orig\NuGet.exe to confirm the issue (run Pack-orig.bat to verify)
        // WARN: you cannot add the patched NuGet executable with a different name (NuGet-patched.exe for example) to the same 
        //       directory that contains the original NuGet.exe, the result will be that the original is always used no matter what!
        public static void Test1(this object source)
        {
        }
    }
}
